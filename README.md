# Description

This is a simple prometheus exporter for collecting usage stats of licenses on an RLM License server. It connects directly to the license server and does not require `rlmutil` installed.

# Dependencies

Python 3
prometheus_client (python module)

# Usage

```bash
rlm_exporter.py conf/example.json
```

# Dashboard

Import the dashboard.json in Grafana

![dashboard](dashboard.png)
